var webapp = angular.module('webapp', ['ui.router']);

   webapp.config(function($stateProvider, $urlRouterProvider) {

       $urlRouterProvider.otherwise('/login');

       $stateProvider

       // HOME STATES AND NESTED VIEWS =======================================
           .state('user', {
               url: '/user',
               templateUrl: 'templates/user.html',
               controller:'userController'
           })

           .state('useredit', {
              url: '/useredit/:userId',
              templateUrl: 'templates/useredit.html',
              controller:'userController'
           })


           .state('home', {
               url: '/home',
               templateUrl: 'templates/home.html'
           })

           .state('tables', {
               url: '/tables',
               templateUrl: 'templates/tables.html'
           })

           .state('companyprofile', {
                url: '/companyprofile/:userId',
                templateUrl: 'templates/companyprofile.html',
                controller:'companyProfileController'
           })

           .state('tableview', {
               url: '/tableview',
               templateUrl: 'templates/viewtable.html'
          })

           .state('uploaded', {
              url: '/uploaded',
              templateUrl: 'templates/upload.html'
         })

           .state('imagecapture', {
               url: '/imagecapture',
               templateUrl: 'templates/imagecapture.html'
          })

           // ABOUT PAGE AND MULTIPLE NAMED VIEWS =================================
           .state('login', {
               url: '/login',
               templateUrl: 'templates/login.html',
               controller:'userController'


               // we'll get to this in a bit
           });

   });