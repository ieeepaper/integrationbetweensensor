webapp.factory('userServices',function($http){

    var postAllRegisterDetails = function(data)
    {
       return $http.post('/userRegistration', data);
    };

    var postAllLoginDetails = function(data)
    {
        console.log("testing:::::", data)
       return $http.post('/login', data);
    };

    var getSingleprofile=function(userId)
        {
           return $http.get('/userBymongoId/'+userId);
        }

    var getAllProfileDetails=function()
        {
           return $http.get('/AllProfiletDetails');
        }
    var deleteProfile=function(userId)
          {
             return $http.delete('/userBymongoId/'+userId);
          }
     var updateprofile=function(editdata)
         {
            return $http.post('/editProfileBymongoId',editdata);
         }

    return {
      postAllRegisterDetails:postAllRegisterDetails,
      postAllLoginDetails: postAllLoginDetails,
      getSingleprofile:getSingleprofile,
      getAllProfileDetails:getAllProfileDetails,
      deleteProfile:deleteProfile,
      updateprofile:updateprofile
    }

});
