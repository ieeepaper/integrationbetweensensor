var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    UserModel = mongoose.model('user');

    module.exports = function (app){
        app.use('/', router);
    };

router.post('/userRegistration', function(req, res, next) {
    var userModel = new UserModel(req.body);
    userModel.save(function(err, result) {
        if (err){
            console.log('Registration failed: ' + err);
        }
        res.send(result);
    });
});

router.post('/login', function(req, res, next) {
   console.log(">>>>", req.body);
    var email = req.body.email;
    var password =  req.body.password;
    UserModel.findOne({email:email}, {password:1},  function(err, result) {
        if (err){
            res.send({loginStatus: false});
        }
        if(password === result.password ){
            res.send({loginStatus: true});
        }
        else{
            res.send({loginStatus: false});
        }
    });
});

router.get('/userBymongoId/:userMongoId',function(req,res,next){
console.log('userMongoId', req.params.userMongoId);
UserModel.findOne({"_id":req.params.userMongoId},function(err,result){
                    if(err)
                        {
                         console.log(err);
                        }
                     else
                      {
                         console.log(result);
                         res.send(result);

                        }


                       })

});

router.get('/AllProfiletDetails', function(req, res, next) {
UserModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))

})

router.delete('/userBymongoId/:userMongoId',function(req, res, next){
console.log('userMongoId', req.params.userMongoId);
UserModel.remove({"_id":req.params.userMongoId},function(err,result)
{
if(err)
{
 console.log(err);
}
else
{
 res.send(result)
}

});
});


router.post('/editProfileBymongoId', function(req, res, next) {
console.log("******", req.body)
console.log(req.body._id);
UserModel.findOneAndUpdate({"_id":req.body._id},req.body,{upsert: true, new: true},
function(err,result)
    {
        if(err){
            console.log(err.stack)
        }else{
            res.send(result)
        }

    });

})




